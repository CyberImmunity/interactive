#!/bin/bash
set -x

S2EGET=./s2eget
S2EPUT=./s2eput
S2ECMD=./s2ecmd
TOOLS="s2ecmd s2eget s2eput s2e.so"
CB="cb"
SEED_FILE="52ec4973a717cfb3810b47cecac9c22e_id:000640,src:000249+000422,op:splice,rep:8"
SYMB_FILE="/tmp/input"

# update guest tools
for TOOL in ${TOOLS}; do
    ${S2EGET} ${TOOL}
    chmod +x ${TOOL}
done

# Don't print crashes in the syslog. This prevents unnecessary forking in the
# kernel
sudo sysctl -w debug.exception-trace=0

# Prevent core dumps from being created. This prevents unnecessary forking in
# the kernel
ulimit -c 0

# Ensure that /tmp is mounted in memory (if you built the image using s2e-env
# then this should already be the case. But better to be safe than sorry!)
if ! mount | grep "/tmp type tmpfs"; then
    sudo mount -t tmpfs -osize=10m tmpfs /tmp
fi

sudo modprobe s2e

# fetch target cb
${S2EGET} ${CB}
chmod +x "${CB}"

#fetch libraries
${S2EGET} "libasan.so.2"

# fetch input
$S2EGET $SEED_FILE
cp ${SEED_FILE} ${SYMB_FILE}
${S2ECMD} symbfile ${SYMB_FILE}

# execute

${S2ECMD} message "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
MSG=$(S2E_SYM_ARGS="" LD_PRELOAD="./s2e.so" ./cb -agtnucDxpRzwW --dyn-syms ${SYMB_FILE} 2>&1)
S2E_SYM_ARGS="" LD_PRELOAD="./s2e.so" ./cb -agtnucDxpRzwW --dyn-syms ${SYMB_FILE} 2>&1
${S2ECMD} message "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
${S2ECMD} message "++++ ${MSG} ++++"
${S2ECMD} message "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"


# Kill states before exiting
${S2ECMD} kill $? "'cb' state killed"
