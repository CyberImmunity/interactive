s2e = {
    logging = {
        console = "debug",
        logLevel = "debug",
    },
    kleeArgs = { },
}

plugins = {
    "BaseInstructions",
    "TaintSearcher",
    "HostFiles",
    "FunctionModels",
    "LinuxMonitor",
    "Vmi",
    "ModuleExecutionDetector",
    "ExecutionTracer",
    "TestCaseGenerator",
}

pluginsConfig = { }

pluginsConfig.ModuleExecutionDetector = {
    mod_0 = {
        moduleName = "cb",
        kernelMode = false,
    },
    mod_1 = {
        moduleName = "libasan.so.2",
        kernelMode = false,
    },

}

pluginsConfig.TaintSearcher = {
    PathLog = "/tmp",
    fileID = "1922",
}


pluginsConfig.HostFiles = {
    baseDirs = {
          -- directory for the binary
          "/tmp/readelf_x86_64//binary",
          -- directory for the config, bootstrap
          "/tmp/readelf_x86_64//output_s2e/config/52ec4973a717cfb3810b47cecac9c22e",
          -- directory for the test input
          "/tmp/readelf_x86_64//input_s2e",
          -- directory for the tools
          "/opt/s2e/bin/guest-tools64",
          -- directory for the shared libraries
          "/tmp/readelf_x86_64//library",
          "/tmp/readelf_x86_64//file",
    },
    allowWrite = true,
}

pluginsConfig.LinuxMonitor = {
    -- Kill the execution state when it encounters a segfault
    terminateOnSegfault = true,

    -- Kill the execution state when it encounters a trap
    terminateOnTrap = true,
}

pluginsConfig.TestCaseGenerator = {
    generateOnStateKill = true,
    generateOnSegfault = true,
    PathTestCases = "/tmp/readelf_x86_64//output_s2e/testcases",
}
