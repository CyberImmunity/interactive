import argparse
import json
import os

def generate_dict(target):
    import angr
    import networkx as nx
    res = dict()

    proj = angr.Project(target, load_options={'auto_load_libs': False})
    cfg = proj.analyses.CFG()
    nodes = cfg.graph.nodes()

    for node in nodes:
        start = end = node.addr
        if node.size:
            end = start + node.size

        key = '{0:#x},{1:#x}'.format(start, end)
        if key not in res:
            res[key] = list()

        for successor in node.successors:
            start = end = successor.addr
            if successor.size:
                end = start + successor.size
            res[key].append('{0:#x},{1:#x}'.format(start, end))

    return res


def dump_to_file(filename, content):
    with open(filename, 'w+') as f:
        json.dump(content, f, sort_keys=True, indent=4, separators=(',', ': '))


def parse_args():
    """TODO: Docstring for parse_args.
    :returns: TODO

    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--binary', default='')
    parser.add_argument('-o', '--output', default='')

    kwargs = vars(parser.parse_args())

    if not kwargs['output']:
        target_bin = os.path.basename(kwargs['binary'])
        target_dir = os.path.dirname(kwargs['binary'])
        kwargs['output'] = '{}/{}_cfg_angr.json'.format(target_dir, target_bin)

    return kwargs


def inside_docker():
    """ check if the script is running inside docker container """
    return os.path.isfile('/.dockerenv')


def launch_container(argv):
    """ launch docker container """
    import docker

    script_dir = os.path.dirname(os.path.realpath(__file__))
    target_dir = os.path.dirname(os.path.realpath(argv['binary']))
    output_dir = os.path.dirname(os.path.realpath(argv['output']))

    script_name = os.path.basename(__file__)
    target_name = os.path.basename(argv['binary'])
    output_name = os.path.basename(argv['output'])

    if target_dir == output_dir:
        tmp_out = '/tmp/target'
    else:
        tmp_out = '/tmp/output'

    venv = '/home/angr/.virtualenvs/angr/bin/activate'
    cmd_str = 'source {}; python /tmp/script/{} -b /tmp/target/{} -o {}/{}'\
        .format(venv, script_name, target_name, tmp_out, output_name)
    cmd_list = cmd_str.split(';')

    volumes = {}
    volumes[script_dir] = {'bind': '/tmp/script', 'mode': 'ro'}
    volumes[target_dir] = {'bind': '/tmp/target', 'mode': 'rw'}
    if output_dir != target_dir:
        volumes[output_dir] = {'bind': '/tmp/output', 'mode': 'rw'}

    image_name = 'angr/angr:latest'
    client = docker.from_env()

    container = client.containers.run(image_name,
                                      user='angr',
                                      name='angr',
                                      working_dir='/home/angr',
                                      hostname='angr',
                                      volumes=volumes,
                                      auto_remove=True,
                                      command=cmd_list)

    # docker run -h angr --name angr -t --rm \
    #         -v ${VOLUMN}:/tmp/target \
    #         -v ${script_dir}:/home/angr/script \
    #         angr/angr:latest \
    #         bash /home/angr/script/generate_cfg_angr.sh ${TARGET}

def main():
    argv = parse_args()
    if not inside_docker():
        launch_container(argv)
    else:
        content = generate_dict(argv['binary'])
        dump_to_file(argv['output'], content)


if __name__ == "__main__":
    main()
