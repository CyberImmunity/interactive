#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' perform static analyze on binaries and/or dynamic analyze upon binary + test input'''
import argparse
import tempfile
import shlex
import subprocess
import os
import time
from collections import OrderedDict, defaultdict
from multiprocessing import Process, active_children, Queue, Manager
from os.path import basename, dirname
from io import BytesIO
import json
import traceback
from setproctitle import setproctitle
from inotify import adapters
from inotify.constants import IN_CREATE, IN_CLOSE_WRITE, IN_ISDIR
from utils import md5sum, run_command_noret, build_cmd, kill_process, check_dir
import sqldb

import logging
logger = logging.getLogger('analyzer')


class AnalyzeFile(object):
    """Docstring for AnalyzeFile. """

    def __init__(self, qemu, s2e_image, binary, command, db_file):
        """ init """
        self._qemu       = qemu
        self.s2e_image   = s2e_image
        self.binary      = binary
        self._command    = command
        self.db_file = db_file
        self._fifo_name  = ''
        self._tmpdir     = ''


    def parse_trace(self):
        """ read from fifo and parse into dictionary """
        trace_tbs = defaultdict(int)
        trace_edges = defaultdict(int)
        do_mmap = False
        mmap = dict()               # map of loaded processes memory map
        current = None              # current basic block being parsed
        previous = None             # previous basic block beding parsed
        count = 0
        with open(self._fifo_name, 'r') as fifo:
            for line in fifo:
                count += 1
                # the trace file consists of two parts, mmap and exec trace
                # they are seperated by a line of "-"(end) and "+"(start)
                # the markers should always come in pair ...
                if line[0] == '-':
                    do_mmap = False
                    continue
                if line[0] == '+':
                    do_mmap = True
                    continue
                if do_mmap:
                    res = [x.strip(' ') for x in line[:-1].split(':')]
                    # ignore if the third element (filename) is empty
                    if len(res) == 2 or res[2] == '':
                        continue
                    if not res[2] in mmap:
                        mmap[res[2]] = set()
                    mmap[res[2]].add((int(res[0], 16), int(res[1], 16)))
                    continue
                # process traceed tbs
                current = tuple([int(x, 16) for x in line[:-1].split(':')])

                # executed tbs with statistic
                trace_tbs[current] += 1

                edge = (previous, current)
                trace_edges[edge] += 1

                previous = current

        return (trace_tbs, trace_edges, mmap)


    def map_edges(self, trace_edges, mmap):
        """ map edge traces to filename in memory map """

        mmap_edges = dict()
        for filename, m_ranges in mmap.iteritems():
            # skip if the memory mapped object is not a file. eg, "stack"
            if not os.path.isfile(filename):
                continue

            mmap_edges[filename] = dict()

            for m_range in m_ranges:
                mapped_edges = set()
                for edge, value in trace_edges.iteritems():
                    if edge[0] is None or edge[1] is None:
                        continue

                    if edge[0][0] > m_range[0] and edge[0][0] < m_range[1]:
                        mmap_edges[filename]['{0:#x},{1:#x}:{2:#x},{3:#x}'.format(edge[0][0], edge[0][1], edge[1][0], edge[1][1])] = value
                        mapped_edges.add(edge)

                # reduce set during each of the loops
                for edge in mapped_edges:
                    trace_edges.pop(edge, None)

        return mmap_edges


    def map_tbs(self, trace_tbs, mmap):
        """ map basic block traces to filename in memory map """

        mmap_tbs = dict()
        for filename, m_ranges in mmap.iteritems():
            # skip if the memory mapped object is not a file. eg, "stack"
            if not os.path.isfile(filename):
                continue

            mmap_tbs[filename] = dict()

            for m_range in m_ranges:
                mapped_bbs = set()
                for tb, value in trace_tbs.iteritems():
                    if tb[0] > m_range[0] and tb[0] < m_range[1]:
                        mmap_tbs[filename]['{0:#x},{1:#x}'.format(tb[0], tb[1])] = value
                        mapped_bbs.add(tb)

                # reduce set during each of the loops
                for tb in mapped_bbs:
                    trace_tbs.pop(tb, None)

        return mmap_tbs


    def mkfile(self, temp=True, name=None):
        if temp:
            self._fifo_name = tempfile.mkstemp('.fifo', 'tmp', '/tmp/tmpfiles')[1]
        else:
            self._fifo_name = name
            open(self._fifo_name, 'w').close()


    def mkfifo(self, fifo=None):
        """ create FIFO """
        if fifo is None:
            fifo = 'fifo'
        self._tmpdir = tempfile.mkdtemp()
        self._fifo_name = os.path.join(self._tmpdir, fifo)
        try:
            os.mkfifo(self._fifo_name)
        except OSError as excp:
            os.rmdir(self._tmpdir)
            print('Failed to create FIFO')
            print(getattr(excp, 'message', repr(excp)))
            traceback.print_exc()
            raise excp


    @staticmethod
    def get_tb_status(tb, bb_dict):
        """ return the matching result of a translation block(tb)
        return: 1 - no match
                2 - tb spans across multiple bbs
                3 - inside, end does not match
                4 - inside, end does match
                other - db index of matched bb
        """
        if tb in bb_dict:
            return (0, tb)
        if tb[0] == tb[1]:
            return (1, None)
        for bb in bb_dict:
            if tb[0] >= bb[1] or tb[1] <= bb[0]:
                continue
            if tb[1] < bb[1]:
                return (3, bb)
            if tb[1] == bb[1]:
                return (4, bb)
            if tb[1] > bb[1]:
                return (2, None)
        return (1, None)


    def build_qemu_cmd(self, testcase):
        """ build qemu command """
        # library_path = '-E LD_LIBRARY_PATH={}/library'.format(self.basedir)
        # binary_path = '{}/binary'.format(self.basedir)

        library_path = ''
        binary_path = self.binary

        command = self._command[:]

        # replace the first occurance of '@@' in the command from right
        # if no '@@' found, meaning that input should be piped to stdin
        if '@@' in command:
            command[command.index('@@')] = testcase
        else:
            command.append('< {}'.format(testcase))
        exec_cmd = ' '.join(command)

        return '''
               {QEMU} {LIBRARY} -d nochain,tb -D {TRACE} {COMMAND}
               '''.format(QEMU    = self._qemu,
                          LIBRARY = library_path,
                          TRACE   = self._fifo_name,
                          BINARY  = binary_path,
                          COMMAND = exec_cmd)


    def analyze_dynamic(self, testcase, bb_with_id, edge_with_id):
        """ analyze the dynamic translation block coverage with qemu """
        # Execute binary with qemu user mode while taking care of libraries
        # collect dynamic translation block execution information

        # 1. create a named pipe for qemu to write to
        # log_name = '{}.ana'.format(testcase)
        # self.mkfile(False, log_name)
        self.mkfifo()

        # 2. build command and launch QEMU
        cmd = self.build_qemu_cmd(testcase)
        # run_command_noret(cmd, 40, 'ANALYZER')
        process = Process(target=run_command_noret, args=[cmd, 40, 'ANALYZER'])
        process.start()

        # 3. read from fifo after QEMU finished executing
        try:
            trace_tbs, trace_edges, mmap = self.parse_trace()
        except Exception as e:
            traceback.print_exc()
            print('error when parsing qemu trace')
            print(getattr(e, 'message', repr(e)))
            raise e
        finally:
            os.remove(self._fifo_name)
            os.rmdir(self._tmpdir)
            process.join()
            pass

        if not mmap:
            mmap[self.binary] = set()
            mmap[self.binary].add((0, 0x7fffffff))

        # map traced TBs to files in memory map
        mmap_tbs = self.map_tbs(trace_tbs, mmap)
        mmap_edges = self.map_edges(trace_edges, mmap)

        # with open('{}.tb.json'.format(testcase), 'w') as js:
        #     json.dump(mmap_tbs, js, sort_keys=True, indent=4, separators=(',', ': '))
        # with open('{}.edge.json'.format(testcase), 'w') as js:
        #     json.dump(mmap_edges, js, sort_keys=True, indent=4, separators=(',', ': '))

        # store info in database

        db = sqldb.SqlDB(self.db_file)
        id_file = db.update_file(testcase)
        db.update_bb_cov(id_file, mmap_tbs, bb_with_id, self.binary)
        db.update_edge_cov(id_file, mmap_edges, bb_with_id, edge_with_id, self.binary)


def dynamic_analyzer(argv):
    analyzer = AnalyzeFile(qemu=argv['qemu'],
                           s2e_image=argv['s2e'],
                           binary=argv['binary'],
                           command=argv['command'],
                           db_file=argv['db_file'])
    queue = Queue()
    max_testcase_size = argv.get('max_testcase_size', 1024*1024*50)
    processed = set()
    tracker = set()
    manager = Manager()
    bb_with_id = manager.dict(argv['bb_with_id'])
    edge_with_id = manager.dict(argv['edge_with_id'])

    root_path = '/opt/interactive'

    def pin(testcase):
        path_pin = '{}/pin/pin-3.7/pin'.format(root_path)
        path_lh_coverage = '{}/lighthouse/coverage/pin/obj-intel64/CodeCoverage.so'.format(root_path)

        id_testcase = os.path.basename(testcase)[3:9]
        log_dir = '{}/drcov'.format(argv['output_dir'])
        check_dir(log_dir)
        log_name = '{}/{}.drcov'.format(log_dir, id_testcase)

        command = argv['command'][:]
        if '@@' in command:
            command[command.index('@@')] = testcase
        else:
            command.append('< {}'.format(testcase))

        exec_cmd = ' '.join(command)
        pin_cmd = '{} -t {} -l {} -- {}'.format(path_pin, path_lh_coverage, log_name, exec_cmd)

        logger.debug(pin_cmd)
        run_command_noret(pin_cmd, 40, 'PIN')
        pass



    def s2e(testcase):
        arch = 'x86_64'
        path_s2e_lua_template = '/opt/interactive/templates/analyze.lua.template'
        path_s2e_bootstrap_template = '/opt/interactive/templates/bootstrap.sh.template'

        id_testcase = os.path.basename(testcase)[3:9]
        path_s2e_configs = '{}/s2e_config/{}'.format(argv['output_dir'], id_testcase)
        check_dir(path_s2e_configs)

        log_dir = '{}/taint'.format(argv['output_dir'])
        check_dir(log_dir)

        #################################################################################
        s2e_paths = dict()         # contains the paths inside the docker container
        # if self._arch == 'i386':
        #     paths['tools'] = '/opt/s2e/bin/guest-tools32'.format(self._S2EDIR)
        # elif self._arch == 'x86_64':
        s2e_paths['tools'] = '/opt/s2e/bin/guest-tools64'

        # paths['testcases'] = '{}/{}/queue'.format(argv['output_dir'], argv['test'])
        # paths['binary'] = os.path.dirname(argv['binary'])
        # paths['config'] = '{}/s2e_config/{}'.format(argv['output_dir'], id_testcase)

        s2e_paths['testcases'] = '/opt/queue'
        s2e_paths['binary'] = '/opt/binary'
        s2e_paths['config'] = '/opt/config'
        s2e_paths['output'] = '/opt/output'

        modules = ('    mod_1 = {\n'
                      '        moduleName = "cb",\n'
                      '        kernelMode = false,\n'
                      '    },\n')

        with open(path_s2e_lua_template) as f_lua_temp:
            lua_temp = f_lua_temp.read()

        lua_mod = lua_temp.format(FILE_ID      = id_testcase,
                                  MODULES      = modules,
                                  PATH_LOG     = s2e_paths['output'],
                                  PATH_BINARY  = s2e_paths['binary'],
                                  PATH_INPUT   = s2e_paths['testcases'],
                                  PATH_TOOLS   = s2e_paths['tools'],
                                  PATH_CONFIG  = s2e_paths['config'])

        with open(os.path.join(path_s2e_configs, 'analyze.lua'), 'w') as f_lua:
            f_lua.write(lua_mod)

        # build the execute command according to
        # whether the input is from file or stdin ('@@' in command.json)
        command = argv['command'][:]

        # replace the first occurance of '@@' in the command from right
        # if no '@@' found, meaning that input should be piped to stdin
        command[0] = os.path.basename(command[0])       # inside container, no directories
        if '@@' in command:
            command[command.index('@@')] = '${SYMB_FILE}'
        else:
            command.append('< ${SYMB_FILE}')

        exec_cmd = ' '.join(command)

        # files = []
        # for target in glob.glob(os.path.join(paths['file'], '*')):
        #     files.append('{} "{}"'.format('${S2EGET}', os.path.basename(target)))

        lib_cmd = 'LD_PRELOAD="./s2e.so'
        # for lib in glob.glob(os.path.join(paths['library'], '*')):
        #     files.append('{} "{}"'.format('${S2EGET}', os.path.basename(lib)))
        #     lib_cmd += ' ./{}'.format(os.path.basename(lib))
        lib_cmd += '"'

        with open(path_s2e_bootstrap_template) as f_bootstrap_temp:
            bs_temp = f_bootstrap_temp.read()

        bs_mod = bs_temp.format(_CB    = command[0],
                                _INPUT = os.path.basename(testcase),
                                _LIB_CMD = lib_cmd,
                                _CMD   = exec_cmd)

        with open(os.path.join(path_s2e_configs, 'bootstrap.sh'), 'w') as f_bs:
            f_bs.write(bs_mod)



        ###########################################################################

        s2e_env = dict()
        install_dir='/opt/s2e'
        s2e_env['S2E_CONFIG']            = "{}/analyze.lua".format(s2e_paths['config'])
        s2e_env['S2E_OUTPUT_DIR']        = s2e_paths['output']
        s2e_env['S2E_SHARED_DIR']        = "{}/share/libs2e".format(install_dir)
        s2e_env['S2E_MAX_PROCESSES']     = '1'
        s2e_env['S2E_UNBUFFERED_STREAM'] = '1'
        s2e_env['LD_PRELOAD']            = "{}/share/libs2e/libs2e-{}-s2e.so".format(install_dir, arch)
        s2e_env['LD_LIBRARY_PATH']       = '{}/lib:{}'.format(install_dir, '$LD_LIBRARY_PATH')

        qemu_cmd = []
        qemu_cmd.append('{}/bin/qemu-system-{}'.format(install_dir, arch))
        qemu_cmd.append('-drive file=/opt/images/debian-8.7.1-{}/image.raw.s2e,format=s2e,cache=writeback'.format(arch))
        qemu_cmd.append('-k en-us')
        qemu_cmd.append('-nographic')
        qemu_cmd.append('-monitor null')
        qemu_cmd.append('-m 256M')
        qemu_cmd.append('-enable-kvm')
        qemu_cmd.append('-serial file:/dev/null')
        qemu_cmd.append('-net none')
        qemu_cmd.append('-net nic,model=e1000')
        qemu_cmd.append('-loadvm ready')

        cmd = ' '.join(qemu_cmd)

        _docker_cmd = 'docker run -h s2e --rm --name {NAME} {VOLUME} {ENV} {IMAGE} {CMD}'
        container_name = 's2e_taint_{}'.format(id_testcase)
        volume = dict()
        volume['{}/{}/queue'.format(argv['output_dir'], argv['test'])] = s2e_paths['testcases']
        volume[os.path.dirname(argv['binary'])] = s2e_paths['binary']
        volume[path_s2e_configs] = s2e_paths['config']
        volume[log_dir] = s2e_paths['output']

        env_str = ''
        for k, v in s2e_env.iteritems():
            env_str += ' -e {}={} '.format(k, v)

        volume_str = ''
        for k, v in volume.iteritems():
            volume_str += ' -v {}:{}'.format(k, v)

        # the docker image name of the symbolic taint tracing
        image = argv['s2e']

        docker_cmd = _docker_cmd.format(NAME=container_name,
                                        VOLUME=volume_str,
                                        ENV=env_str,
                                        IMAGE=image,
                                        CMD=cmd)

        logger.debug( docker_cmd)

        with open(os.path.join(path_s2e_configs, 'docker_cmd'), 'w') as f_cmd:
            f_cmd.write(docker_cmd)

        run_command_noret(docker_cmd, 40, 'DOCKER')
        # return (s2e_env, cmd)
        # return paths['config']




    def process_input(path, filename):
        ''' need to decouple all the different external calls '''
        setproctitle('analyzing [{}:{}]'.format(basename(dirname(path)), filename))

        testcase = os.path.join(path, filename)
        # analyze bb coverage
        analyzer.analyze_dynamic(testcase, bb_with_id, edge_with_id)

        # use pin to generate lighthouse trace
        pin(testcase)

        # taint tracking with s2e
        s2e(testcase)



    def process_stats(path, filename):
        db = sqldb.SqlDB(argv['db_file'])

        with open(os.path.join(path, filename)) as f_stats:
            content = f_stats.readlines()

        stats_dict = dict(map(str.strip, line.split(':',1)) for line in content)
        columns = stats_dict.keys()
        values = [stats_dict[column] for column in columns]

        # query = 'update fuzzer_stats set {}=? where id=1'.format('=?, '.join(columns))
        # conn, cur = db.execute(query, values)
        # db.close(conn)
        # if cur.lastrowid:
        #     logger.debug('update id is {}'.format( cur.lastrowid))
        #     return

        query = 'insert into fuzzer_stats({}) values ({}?)'\
            .format(', '.join(columns), '?, '*(len(columns)-1))
        conn, cur = db.execute(query, values)
        db.close(conn)


    def worker():
        setproctitle('DISPATCHER')
        processes = []
        MAX_PROCESSES = 1
        logger.debug('analyze worker started')

        while True:
            try:
                # max number of child processes
                while active_children():
                    processes[:] = [p for p in processes if p.is_alive()]
                    if len(processes) < MAX_PROCESSES:
                        break
                    time.sleep(0.1)

                path, filename = queue.get()

                p_input = Process(target=process_input, args=[path, filename])
                p_input.start()
                processes.append(p_input)
            except KeyboardInterrupt:
                raise
            except Exception as excp:
                logger.debug(getattr(excp, 'message', repr(excp)))
                continue


    worker = Process(target=worker)
    worker.start()

    monitor_dir = os.path.join(argv['output_dir'], argv['test'])
    check_dir(monitor_dir)
    logger.debug('monitoring {}'.format(monitor_dir))

    i = adapters.InotifyTree(monitor_dir, mask=IN_CLOSE_WRITE)
    for event in i.event_gen():
        if event is None:
            continue
        (_, _, path, filename) = event

        if filename == '.cur_input':
            continue

        # filter #1, most often: the event is not inside queue directory
        dir_base = basename(path)
        if dir_base != 'queue':
            # possibly its the fuzzer statistics
            if filename == 'fuzzer_stats':
                # workaround for firing IN_CLOSE_WRITE twice
                if not (path, filename) in tracker:
                    tracker.add((path, filename))
                    continue
                tracker.remove((path, filename))

                p_stats = Process(target=process_stats, args=[path, filename])
                p_stats.start()
            continue

        # filter #2, there is a subdirectory inside queue
        if not filename.startswith('id:'):
            continue

        if filename.endswith('json') or filename.endswith('ana'):
            continue

        # filter #3, do not analyze seedbox
        node = basename(dirname(path))
        if node == 'seedbox':
            continue

        # filter #4, for *MOST* of the test case AFL created,
        # IN_CLOSE_WRITE event will fire twice, workaround by only handling
        # the event every second time.
        if not (path, filename) in tracker:
            tracker.add((path, filename))
            continue
        tracker.remove((path, filename))

        current = []
        # XXX since the tracker set keeps growing, clear the set when reaches
        # 100 records by try to put them into queue for processing
        if len(tracker) > 100:
            while tracker:
                current.append(tracker.pop())

        # always put current event file if reach here
        current.append((path, filename))

        for c_path, c_filename in current:
            # filter #5, different nodes can generate test case with same hash
            # md5 = md5sum(os.path.join(c_path, c_filename))
            # if md5 in processed:
            #     continue
            # processed.add(md5)

            if c_filename in processed:
                continue
            processed.add(c_filename)

            # filter #6, skip large testcases
            f_size = os.stat(os.path.join(c_path, c_filename)).st_size
            if f_size > max_testcase_size:
                continue

            queue.put((c_path, c_filename))

            logger.debug('[{}][{}]: [{}]'.\
                         format(len(processed), basename(dirname(c_path)), c_filename))
        active_children()

