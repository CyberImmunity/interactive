#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os
import glob
import time
import subprocess
from os.path import basename, dirname
from multiprocessing import Process, active_children, Queue
import logging
from logging.config import fileConfig

from analyze import dynamic_analyzer
from utils import run_command_noret, run_command_ret, md5sum, check_dir, objdump, parse
import sqldb

fileConfig('logging.conf')
logger = logging.getLogger("master")

# logger.debug('debug message')
# logger.info('info message')
# logger.warn('warn message')
# logger.error('error message')
# logger.critical('critical message')


def gen_cfg_angr(argv):
    argv['target'] = '../tests/cb'
    argv['output'] = ''
    cmd = 'bash {SCRIPT} {TARGET} {OUTPUT}'\
        .format(SCRIPT='./generate_cfg_angr.sh',
                TARGET=argv['target'],
                OUTPUT=argv['output'])
    run_command_noret(cmd)



def launch_afl(argv):
    # /opt/afl/afl-fuzz -i /opt/afl/testcases/others/elf -o ./output -m none -S test1 -- /opt/angora/tests/readelf/readelf_afl -g @@
    cmd = '{} -Q -i {} -o {} -m none -S {} -- {}'\
        .format(argv['afl'],
                argv['input_dir'],
                argv['output_dir'],
                argv['test'],
                ' '.join(argv['command']))

    env = {}
    env['AFL_NO_UI'] = '1'
    logger.debug('launching AFL with command:')
    logger.debug(cmd)
    run_command_noret(cmd, env=env)

    pass


def accept_target_location():
    pass


def update_fuzzer_stats():
    pass



def check_kernel():
    update_kernel = 'please check AFL kernel settings [{}]'
    # /proc/sys/kernel/sched_child_runs_first
    check_1 = '/proc/sys/kernel/sched_child_runs_first'
    check_1_result = open(check_1).read().strip('\n')
    if check_1_result != '1':
        print(update_kernel.format('sched_child_runs_first'))
        exit(1)

    # /proc/sys/kernel/core_pattern
    check_2 = '/proc/sys/kernel/core_pattern'
    check_2_result = open(check_2).read().strip('\n')
    if check_2_result != 'core':
        print(update_kernel.format('core_pattern'))
        exit(1)

    # /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
    check_3_dir = '/sys/devices/system/cpu/cpu*/cpufreq/scaling_governor'
    check_3_result = set()
    for check_3 in glob.glob(check_3_dir):
        check_3_result.add(open(check_3).read().strip('\n'))

    if len(check_3_result) != 1:
        print(check_3_result)
        print(update_kernel.format('scaling_governor'))
        exit(1)


def main(argv):
    basedir = '/opt/interactive/'
    argv['s2e'] = 's2e_taint'   # docker image name of s2e taint tracer
    argv['afl'] = os.path.abspath(os.path.relpath('{}/afl/afl-fuzz'.format(basedir)))
    argv['qemu'] = os.path.abspath(os.path.relpath('{}/coverage/qemu-x86_64'.format(basedir)))
    argv['binary'] = os.path.abspath(os.path.relpath('{}/tests/cb'.format(basedir)))
    argv['input_dir'] = '/opt/afl/testcases/others/elf'
    argv['output_dir'] = os.path.abspath(os.path.relpath('{}/tests/output/'.format(basedir)))
    argv['test'] = 'test1'
    argv['command'] = [argv['binary'], '-a', '@@']
    argv['db_name'] = 'test.db'
    argv['db_file'] = os.path.join(argv['output_dir'], argv['db_name'])

    # check for AFL kernel settings
    check_kernel()

    # generating CFG for the binary
    # gen_cfg_angr(argv)

    check_dir(argv['output_dir'])
    db = sqldb.SqlDB(argv['db_file'])
    db.create_tables()
    bb_with_id, edge_with_id = db.update_cfg('{}/tests/cb_cfg_angr.json'.format(basedir))

    # update database instruction information
    argv['objdump'] = '{}.objdump'.format(argv['binary'])
    objdump(argv['binary'], argv['objdump'])
    db.update_instructions(parse(argv['objdump']))

    # launch AFL
    process_afl = Process(target=launch_afl, args=[argv])
    process_afl.start()

    # launch analyzer
    argv['bb_with_id'] = bb_with_id
    argv['edge_with_id'] = edge_with_id
    tc_analyzer = Process(target=dynamic_analyzer, args=[argv])
    tc_analyzer.start()

    # launch flask
    flask_args = {
        'port': 8080,
        'host': '0.0.0.0',
        'use_reloader': False,
    }
    os.environ["DB_FILE"] = argv['db_file']
    from web import app
    web = Process(target=app.run, kwargs=flask_args)
    web.start()

    # launch service waiting for target address
    # watch changes on fuzzer_stats
    # watch changes on queue


def setup_argparse():
    parser = argparse.ArgumentParser()

    parser.add_argument('--fid', type=str)
    parser.add_argument('--uid', type=int, default=0)
    parser.add_argument('--fuzz_lib', action='store_true', default=False)

    args = parser.parse_args()
    kwargs = vars(args)

    return kwargs

if __name__ == "__main__":
    argv = setup_argparse()
    print('*'*100)
    main(argv)
