#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlite3
from sqlite3 import Error
import json
import os
import logging
logger = logging.getLogger('db')

class SqlDB(object):
    """Docstring for SqlDB. """
    def __init__(self, db_name):
        """TODO: to be defined1. """
        self.db_name = db_name

    def create_connection(self):
        """ create a database connection to the SQLite database
            specified by the db_file
        :param db_file: database file
        :return: Connection object or None
        """
        try:
            conn = sqlite3.connect(self.db_name)
            return conn
        except Error as e:
            logger.debug(e)

        return None


    def close(self, conn):
        if conn:
            conn.close()

    def get_bbs(self):
        sql = '''select * from bb'''
        conn, cur = self.execute(sql)
        records = cur.fetchall()
        self.close(conn)
        bb_with_id = dict()
        for record in records:
            bb_with_id[(record[1], record[2])] = record[0]

        return bb_with_id

    def get_edges(self):
        sql = '''select * from edge'''
        conn, cur = self.execute(sql)
        records = cur.fetchall()
        self.close(conn)
        edge_with_id = dict()
        for record in records:
            edge_with_id[(record[1], record[2])] = record[0]

        return edge_with_id


    def executemany(self, sql, target=None):
        try:
            conn= self.create_connection()
            try:
                cur = conn.cursor()
                cur.executemany(sql, target)
                conn.commit()
                return (conn, cur)
            except Error as e:
                conn.rollback()
                logger.debug(sql)
                logger.debug (target)
                logger.debug(e)
        except Error as e:
                logger.debug(e)

        return (None, None)


    def execute(self, sql, target=None):
        try:
            conn= self.create_connection()
            try:
                cur = conn.cursor()
                if target:
                    cur.execute(sql, target)
                else:
                    cur.execute(sql)
                conn.commit()
                return (conn, cur)
            except Error as e:
                conn.rollback()
                logger.debug(sql)
                logger.debug (target)
                logger.debug(e)
        except Error as e:
                logger.debug(e)

        return (None, None)


    def executescript(self, sql, target=None):
        try:
            conn= self.create_connection()
            try:
                cur = conn.cursor()
                if target:
                    cur.executescript(sql, target)
                else:
                    cur.executescript(sql)
                conn.commit()
                return (conn, cur)
            except Error as e:
                conn.rollback()
                logger.debug(sql)
                logger.debug (target)
                logger.debug(e)
        except Error as e:
                logger.debug(e)

        return (None, None)


    def create_tables(self):
        sql = open('./schema.sql').read()
        conn, _ = self.executescript(sql)
        self.close(conn)
        pass


    def insert_single_bb(self, bb):
        # sql = '''insert into bb(start_addr, end_addr) values (?,?)'''

        sql = '''update bb set end_addr=? where start_addr=?'''
        conn, cur = self.execute(sql, bb)
        if cur:
            record_id = cur.lastrowid
            self.close(conn)
            return record_id
        else:
            sql = '''insert or ignore into bb(start_addr, end_addr) values (?,?)'''
            conn, cur = self.execute(sql, bb)
            record_id = cur.lastrowid
            self.close(conn)
            return record_id


    def to_tuple(self, bb_str):
        return tuple([int(x, 16) for x in bb_str.split(',')])


    def update_bb_cov(self, id_file, mmap_tbs, bb_with_id, target=None):
        rec_list = list()
        if target:
            target_tbs = mmap_tbs[target]

            for tb,count in target_tbs.items():
                bb = self.to_tuple(tb)
                bb_id = bb_with_id.get(bb)

                if bb_id is None:
                    bb_id = bb_with_id[bb] = self.insert_single_bb(bb)
                rec_list.append((id_file, bb_id, count))

            sql = '''insert into bb_cov(id_file, id_bb, count) values (?,?,?)'''
            conn, _ = self.executemany(sql, rec_list)
            self.close(conn)
        else:
            pass


    def update_edge_cov(self, id_file, mmap_edges, bb_with_id, edge_with_id, target=None):
        rec_list = list()
        if target:

            target_edges = mmap_edges[target]
            for edge, count in target_edges.items():
                bbs = edge.split(':')
                from_bb = self.to_tuple(bbs[0])
                from_bb_id = bb_with_id.get(from_bb)
                to_bb = self.to_tuple(bbs[1])
                to_bb_id = bb_with_id.get(to_bb)

                if to_bb_id is None:
                    continue

                if from_bb_id is None:
                    from_bb_id = bb_with_id[from_bb] = self.insert_single_bb(from_bb)

                edge = (from_bb_id, to_bb_id)
                edge_id = edge_with_id.get(edge)
                if edge_id is None:
                    sql = '''insert into edge(from_bb, to_bb) values (?,?)'''
                    conn, cur = self.execute(sql, edge)
                    edge_id = edge_with_id[edge] = cur.lastrowid
                    self.close(conn)
                rec_list.append((id_file, edge_id, count))

            sql = '''insert into edge_cov(id_file, id_edge, count) values (?,?,?)'''
            conn, _ = self.executemany(sql, rec_list)
            self.close(conn)

            pass
        else:
            pass


    def update_stats(self):
        pass


    def update_file(self, input_file):
        fname = os.path.basename(input_file)
        with open(input_file) as f:
            blob = f.read()
            sql = '''insert into testcase(name, content) values (?,?)'''
            conn, cur = self.execute(sql, [fname, sqlite3.Binary(blob)])
            record_id = cur.lastrowid
            self.close(conn)
            return record_id


    def update_cfg(self, cfg_json):
        with open(cfg_json) as f_cfg:
            cfg = json.load(f_cfg)

        # BBs
        bb_list = set()
        for k,v in cfg.items():
            bb = self.to_tuple(k)
            bb_list.add(bb)
            for t in v:
                bb = self.to_tuple(t)
                bb_list.add(bb)

        sql = '''insert into bb(start_addr, end_addr) values (?,?)'''
        conn, _ = self.executemany(sql, bb_list)
        self.close(conn)

        # get the ids
        bb_with_id = self.get_bbs()
        # edges
        edge_list = set()
        for k, v in cfg.items():
            if len(v) == 0:
                continue

            from_bb = self.to_tuple(k)
            from_bb_id = bb_with_id.get(from_bb)
            for t in v:
                to_bb = self.to_tuple(t)
                to_bb_id = bb_with_id.get(to_bb)
                edge_list.add((from_bb_id, to_bb_id))

        sql = '''insert into edge(from_bb, to_bb) values (?,?)'''
        conn, _ = self.executemany(sql, edge_list)
        self.close(conn)

        # get the ids
        edge_with_id = self.get_edges()
        return (bb_with_id, edge_with_id)


    def update_instructions(self, instructions):
        sql = 'select * from bb order by start_addr asc'
        conn, cur = self.execute(sql)
        bbs = cur.fetchall()
        self.close(conn)

        # ######################
        # rec = []
        # idx_bb = idx_ins = 0
        # while idx_ins < len(instructions) and idx_bb < len(bbs):
        #     addr, opcode, instruction  = instructions[idx_ins]
        #     cur_bb = bbs[idx_bb]
        #     ins_addr = int(addr, 16)

        #     if ins_addr > cur_bb[2]:
        #         idx_bb+=1
        #         continue

        #     if ins_addr < cur_bb[1]:
        #         idx_ins += 1

        #     if ins_addr >= cur_bb[1]:
        #         rec.append((opcode, instruction, cur_bb[0]))
        #         if ins_addr == cur_bb[2]:
        #             idx_bb += 1
        #         else:
        #             idx_ins += 1
        # print len(rec)
        ########################

        logger.debug('number of instructions to process: {}'.format(len(instructions)))
        count = 0

        rec = []
        for addr, opcode, instruction in instructions:
            count += 1
            if count % 1000 == 0:
                logger.debug('{} processed'.format(count))
            ins_addr = int(addr, 16)

            for bb in bbs:
                if ins_addr >= bb[1] and ins_addr <= bb[2]:
                    rec.append((opcode, instruction, bb[0]))

        logger.debug('total number of records processed: {} '.format(len(rec)))
        ########################
        sql = 'insert into instructions (opcode, instruction, id_bb) values(?, ?, ?)'
        conn, _ = self.executemany(sql, rec)
        self.close(conn)


def main():
    """TODO: Docstring for main.
    :returns: TODO

    """
    from logging.config import fileConfig
    fileConfig('logging.conf')
    db = SqlDB('test.db')
    db.create_tables()
    db.update_cfg('../tests/cb_cfg_angr.json')

    out_dir = '''/opt/interactive/scripts/output/test1/queue/'''

    for _,_,files in os.walk(out_dir):
        for f in sorted(files):
            testcase = os.path.join(out_dir, f)

            binary = '''/opt/interactive/tests/cb'''
            tb_json = '{}.tb.json'.format(testcase)
            edge_json = '{}.edge.json'.format(testcase)


            if not os.path.isfile(tb_json) or not os.path.isfile(edge_json):
                logger.debug('pass {}'.format(f))
                continue

            logger.debug('processing {}'.format(f))
            with open(tb_json) as f:
                mmap_tbs = json.load(f)
            with open(edge_json) as f:
                mmap_edges = json.load(f)

            id_file = db.update_file(testcase)
            db.update_bb_cov(id_file, mmap_tbs, binary)
            db.update_edge_cov(id_file, mmap_edges, binary)
        break


if __name__ == "__main__":
    main()
