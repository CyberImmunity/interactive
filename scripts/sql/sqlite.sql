BEGIN TRANSACTION;
CREATE TABLE `bb` (
  `IDBB` integer NOT NULL
,  `START_ADDR` integer NULL
,  `END_ADDR` integer NULL
,  PRIMARY KEY(`IDBB`)
,  UNIQUE(`START_ADDR`, `END_ADDR`) ON CONFLICT IGNORE
);
CREATE TABLE `edge` (
  `IDEDGE` integer NOT NULL
,  `FROM_BB` integer NULL
,  `TO_BB` integer NULL
,  PRIMARY KEY(`IDEDGE`)
,  UNIQUE(`FROM_BB`, `TO_BB`) ON CONFLICT IGNORE
,    FOREIGN KEY(`FROM_BB`)    REFERENCES `bb` (`IDBB`)
,    FOREIGN KEY(`TO_BB`)    REFERENCES `bb` (`IDBB`)
);
CREATE INDEX `fk_edge_1_idx` ON `edge`(`FROM_BB`);
CREATE INDEX `fk_edge_2_idx` ON `edge`(`TO_BB`);
CREATE TABLE `testcase` (
  `IDFILE` integer NOT NULL
,  `NAME` VARCHAR(256) NULL
,  `CONTENT` BLOB NULL
,  PRIMARY KEY(`IDFILE`)
);
CREATE TABLE `bb_cov` (
  `IDBB_COV` integer NOT NULL
,  `ID_FILE` integer NULL
,  `ID_BB` integer NULL
,  `COUNT` integer NULL
,  PRIMARY KEY(`IDBB_COV`)
,    FOREIGN KEY(`ID_FILE`)    REFERENCES `testcase` (`IDFILE`)
,    FOREIGN KEY(`ID_BB`)    REFERENCES `bb` (`IDBB`)
);
CREATE INDEX `fk_bb_cov_1_idx` ON `bb_cov`(`ID_FILE`);
CREATE INDEX `fk_bb_cov_2_idx` ON `bb_cov`(`ID_BB`);
CREATE TABLE `edge_cov` (
  `IDEDGE_COV` integer NOT NULL
,  `ID_FILE` integer NULL
,  `ID_EDGE` integer NULL
,  `COUNT` integer NULL
,  PRIMARY KEY(`IDEDGE_COV`)
,    FOREIGN KEY(`ID_FILE`)    REFERENCES `testcase` (`IDFILE`)
,    FOREIGN KEY(`ID_EDGE`)    REFERENCES `edge` (`IDEDGE`)
);
CREATE INDEX `fk_edge_cov_1_idx` ON `edge_cov`(`ID_FILE`);
CREATE INDEX `fk_edge_cov_2_idx` ON `edge_cov`(`ID_EDGE`);
create table `fuzzer_stats` (
    `IDFUZZER_STATS` integer NOT NULL,
    `START_TIME` integer NOT NULL,
    `LAST_UPDATE` integer NOT NULL,
    `FUZZER_PID` integer NOT NULL,
    `CYCLES_DONE` integer NOT NULL,
    `EXECS_DONE` integer NOT NULL,
    `EXECS_PER_SEC` varchar(10) not null,
    `PATHS_TOTAL` integer NOT NULL,
    `PATHS_FAVORED` integer NOT NULL,
    `PATHS_FOUND` integer NOT NULL,
    `PATHS_IMPORTED` integer NOT NULL,
    `MAX_DEPTH` integer NOT NULL,
    `CUR_PATH` integer NOT NULL,
    `PENDING_FAVS` integer NOT NULL,
    `PENDING_TOTAL` integer NOT NULL,
    `VARIABLE_PATHS` integer NOT NULL,
    `STABILITY` varchar(10) not null,
    `BITMAP_CVG` varchar(10) not null,
    `UNIQUE_CRASHES` integer NOT NULL,
    `UNIQUE_HANGS` integer NOT NULL,
    `LAST_PATH` integer NOT NULL,
    `LAST_CRASH` integer NOT NULL,
    `LAST_HANG` integer NOT NULL,
    `EXECS_SINCE_CRASH` integer NOT NULL,
    `EXEC_TIMEOUT` integer NOT NULL,
    `AFL_BANNER` varchar(32) not null,
    `AFL_VERSION` varchar(10) not null,
    `TARGET_MODE` varchar(32) not null,
    `COMMAND_LINE` varchar(512) not null,
    PRIMARY KEY (`IDFUZZER_STATS`)
);
CREATE TABLE `targets` (
  `IDTARGET_ADDRESS` integer NOT NULL
,  `target_address` integer NULL
,  `ID_BB` integer NULL
,  PRIMARY KEY(`IDTARGET_ADDRESS`)
,  UNIQUE(`ID_BB`) ON CONFLICT IGNORE
,    FOREIGN KEY(`ID_BB`)    REFERENCES `bb` (`IDBB`)
);
CREATE INDEX `fk_targets_1_idx` ON `targets`(`ID_BB`);
END TRANSACTION;
