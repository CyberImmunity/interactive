"""
Flask Documentation:     http://flask.pocoo.org/docs/
Jinja2 Documentation:    http://jinja.pocoo.org/2/documentation/
Werkzeug Documentation:  http://werkzeug.pocoo.org/documentation/
This file creates your application.
"""

from web import app
from flask import render_template, request, redirect, url_for, flash, send_from_directory
from forms import UserForm
import json
import sqlite3
import os
import time

import random

###
# Routing for your application.
###

@app.route('/')
def home():
    return render_template('home.html')


def recursive_append_node(res, cfg, node, depth):
    if depth > 0:
        depth = depth - 1
        if node not in cfg:
            return res

        res[node] = cfg.get(node)
        for parent in cfg[node]['Edge']:
            res = recursive_append_node(res, cfg, parent, depth)

    return res


def recursive_append_parent(cfg, node_id, depth, bb_cov):
    parent_data = dict()
    try:
        node = cfg[node_id]
        parent_data['name'] = '0x{:0x}:0x{:0x}'.format(node['start_addr'][0], node['end_addr'][0])

        if depth == 0:
            # parent_data['size'] = bb_cov.get(node_id, 1)
            parent_data['size'] = 1
            return parent_data

        parent_data['children'] = list()
        parents = node['Edge']
        for parent in parents:
            parent_data['children'].append(recursive_append_parent(cfg, parent, depth-1, bb_cov))
    except KeyError:
        print node_id

    return parent_data

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                                'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/graph_full')
def graph_full():
    # filename = 'small.json'
    filename = 'new.json'
    json_file = os.path.join(app.config['basedir'], filename)

    with open(json_file) as f:
        data = json.load(f)

    # retrieve nodes and edges
    db=sqlite3.connect(app.config['DB_FILE'])
    cur = db.cursor()

    query = 'select * from bb'
    cur.execute(query)
    bbs = cur.fetchall()

    query = ('select edge.* '
             'from edge, edge_cov '
             'where idedge=edge_cov.id_edge '
             'group by edge_cov.id_edge')
    cur.execute(query)
    edges = cur.fetchall()

    # convert database records to dict
    dict_edge = dict()
    for idedge, from_bb, to_bb in edges:
        if not to_bb in dict_edge:
            dict_edge[to_bb] = set()
        dict_edge[to_bb].add(from_bb)

    # get the coverage
    query = ('select id_bb, sum(count) '
             'from bb_cov '
             'group by id_bb')
    cur.execute(query)
    bb_cov = dict(cur.fetchall())

    cfg = dict()
    for idbb, start_addr, end_addr in bbs:
        node = dict()
        node['record_id'] = [idbb]
        node['start_addr'] = [start_addr]
        node['end_addr'] = [end_addr]
        node['Edge'] = list(dict_edge.get(idbb, set()))
        if len(node['Edge'])>0:
            node['count'] = [bb_cov.get(idbb, 0)]
            cfg[idbb] = node

    query = ('select * from instructions order by idinstructions asc')
    instructions = cur.execute(query).fetchall()

    ins_dict = dict()
    for idins, opcode, instruction, id_bb in instructions:
        if not id_bb in ins_dict:
            ins_dict[id_bb] = list()
        ins_dict[id_bb].append(instruction)

    for node in cfg:
        cfg[node]['Instructions'] = ins_dict.get(node, [])

    data = list(cfg.values())

    return render_template('graph_full.html', data=data, targets=[])


@app.route('/graph_sunburst')
def graph_sunburst():
    # filename = 'small.json'
    filename = 'new.json'
    json_file = os.path.join(app.config['basedir'], filename)

    #current_time = time.time()
    ###############################
    #new_time = time.time()
    #print(new_time - current_time)
    #current_time = new_time
    ###############################

    with open(json_file) as f:
        data = json.load(f)

    # retrieve nodes and edges
    db=sqlite3.connect(app.config['DB_FILE'])
    cur = db.cursor()

    # targets
    cur.execute('select idtargets, target_address, id_bb from targets')
    targets = cur.fetchall()
    if not targets:
        return render_template('graph_sunburst.html', sunburst_data={})


    # bbs
    query = 'select idbb, start_addr, end_addr from bb'
    bbs = cur.execute(query).fetchall()

    # edges
    query = 'select idedge, from_bb, to_bb  from edge'
    edges = cur.execute(query).fetchall()

    # coverage
    query = 'select id_bb, sum(count) from bb_cov group by id_bb'
    bb_cov = dict(cur.execute(query).fetchall())

    # instructions
    query = ('select * from instructions order by idinstructions asc')
    instructions = cur.execute(query).fetchall()

    # convert edge information to dictionary {child: (parents)}
    dict_edge = dict()
    for idedge, from_bb, to_bb in edges:
        if not to_bb in dict_edge:
            dict_edge[to_bb] = set()
        dict_edge[to_bb].add(from_bb)

    # control flow graph represented with nodes
    cfg = dict()
    for idbb, start_addr, end_addr in bbs:
        node = dict()
        node['record_id'] = [idbb]
        node['start_addr'] = [start_addr]
        node['end_addr'] = [end_addr]
        node['Edge'] = list(dict_edge.get(idbb, set()))
        cfg[idbb] = node

    # convert instruction information to dictionary {bb: [instructions]}
    ins_dict = dict()
    for idins, opcode, instruction, id_bb in instructions:
        if not id_bb in ins_dict:
            ins_dict[id_bb] = list()
        ins_dict[id_bb].append(instruction)


    sunburst_data = dict()
    depth = 5
    for target in targets:
        node = dict()
        node['name'] = '0x{:0x}'.format(target[1])
        node['children'] = list()

        parents = cfg[target[2]]['Edge']
        for parent in parents:
            node['children'].append(recursive_append_parent(cfg, parent, depth, bb_cov))

        sunburst_data[target[1]] = node

    # __import__('pprint').pprint(sunburst_data)

    return render_template('graph_sunburst.html', sunburst_data=next(iter(sunburst_data.values())))




@app.route('/graph_topology')
def graph_topology():
    # filename = 'small.json'
    filename = 'new.json'
    json_file = os.path.join(app.config['basedir'], filename)

    #current_time = time.time()
    ###############################
    #new_time = time.time()
    #print(new_time - current_time)
    #current_time = new_time
    ###############################

    with open(json_file) as f:
        data = json.load(f)

    # retrieve nodes and edges
    db=sqlite3.connect(app.config['DB_FILE'])
    cur = db.cursor()

    # targets
    cur.execute('select idtargets, target_address, id_bb from targets')
    targets = cur.fetchall()
    if not targets:
        return render_template('graph_topology.html', data={})


    # bbs
    query = 'select idbb, start_addr, end_addr from bb'
    bbs = cur.execute(query).fetchall()

    # edges
    query = 'select idedge, from_bb, to_bb  from edge'
    edges = cur.execute(query).fetchall()

    # coverage
    query = 'select id_bb, sum(count) from bb_cov group by id_bb'
    bb_cov = dict(cur.execute(query).fetchall())

    # instructions
    query = ('select * from instructions order by idinstructions asc')
    instructions = cur.execute(query).fetchall()

    for ins in instructions:
        if ins[2][0] == ' ':
            print ins[2]
        if ';' in  ins[2]:
            print ins[2]

    

    # convert edge information to dictionary {child: (parents)}
    dict_edge = dict()
    for idedge, from_bb, to_bb in edges:
        if not to_bb in dict_edge:
            dict_edge[to_bb] = set()
        dict_edge[to_bb].add(from_bb)

    # convert instruction information to dictionary {bb: [instructions]}
    ins_dict = dict()
    for idins, opcode, instruction, id_bb in instructions:
        if not id_bb in ins_dict:
            ins_dict[id_bb] = list()
        ins_dict[id_bb].append(instruction)

    # control flow graph represented with nodes
    cfg = dict()
    for idbb, start_addr, end_addr in bbs:
        node = dict()
        node['record_id'] = [idbb]
        node['start_addr'] = [start_addr]
        node['end_addr'] = [end_addr]
        node['Edge'] = list(dict_edge.get(idbb, set()))
        cfg[idbb] = node

    def rec_append_data(dst, data, dict_edge, bb_cov, depth, added):

        if depth > 0:
            depth = depth - 1

            for src in dict_edge.get(dst):
                if src is None:
                    continue

                if src in added:
                    continue
                added.add(src)

                # relation to the dst
                rel = dict()
                rel['dst'] = dst
                rel['src'] = src
                rel['type'] = 'peer'
                rel['customerConeSize'] = bb_cov.get(src, 1)
                # rel['customerConeSize'] = random.randint(1, 10**(7+1))
                data['relationships'].append(rel)

                # src node itself
                node = dict()
                node['asn'] = src
                node['customerConeSize'] = random.randint(10**(depth), 10**(depth+1))
                node['start_addr'] = '0x{:0x}'.format(cfg.get(src)['start_addr'][0])
                node['end_addr'] = '0x{:0x}'.format(cfg.get(src)['end_addr'][0])
                node['ins'] = ins_dict.get(src, list())
                node['lat'] = random.uniform(-180, 180)
                node['lon'] = random.uniform(-180, 180)
                node['size'] = depth*2
                data['ases'].append(node)

                added.add(src)

                if not src in dict_edge:
                    return

                for parent in dict_edge.get(src):
                    if parent in added:
                        continue

                    if parent is None:
                        return

                    rec_append_data(src, data, dict_edge, bb_cov, depth, added)

    depth = 7
    data = {'ases': list(), 'relationships': list()}
    added = set()
    for target in targets:
        added.add(target[2])
        node = dict()
        node['asn'] = target[2]
        node['customerConeSize'] = random.randint(10**(depth), 10**(depth+1))
        node['start_addr'] = '0x{:0x}'.format(cfg.get(target[2])['start_addr'][0])
        node['end_addr'] = '0x{:0x}'.format(cfg.get(target[2])['end_addr'][0])
        node['ins'] = ins_dict.get(target[2], list())
        node['size'] = depth*2
        node['lat'] = random.uniform(-180, 180)
        node['lon'] = random.uniform(-180, 180)
        data['ases'].append(node)

        rec_append_data(target[2], data, dict_edge, bb_cov, depth, added)

    return render_template('graph_topology.html', data=data)


@app.route('/graph')
def graph():
    # filename = 'small.json'
    filename = 'new.json'
    json_file = os.path.join(app.config['basedir'], filename)

    #current_time = time.time()
    ###############################
    #new_time = time.time()
    #print(new_time - current_time)
    #current_time = new_time
    ###############################

    with open(json_file) as f:
        data = json.load(f)

    # retrieve nodes and edges
    db=sqlite3.connect(app.config['DB_FILE'])
    cur = db.cursor()

    # targets
    cur.execute('select idtargets, target_address, id_bb from targets')
    targets = cur.fetchall()
    if not targets:
        return render_template('graph.html', data=[], targets=targets)

    # bbs
    query = 'select idbb, start_addr, end_addr from bb'
    bbs = cur.execute(query).fetchall()

    # edges
    query = 'select idedge, from_bb, to_bb  from edge'
    edges = cur.execute(query).fetchall()

    # coverage
    query = 'select id_bb, sum(count) from bb_cov group by id_bb'
    bb_cov = dict(cur.execute(query).fetchall())

    # instructions
    query = ('select * from instructions order by idinstructions asc')
    instructions = cur.execute(query).fetchall()

    # convert edge information to dictionary {child: (parents)}
    dict_edge = dict()
    for idedge, from_bb, to_bb in edges:
        if not to_bb in dict_edge:
            dict_edge[to_bb] = set()
        dict_edge[to_bb].add(from_bb)

    # control flow graph represented with nodes
    cfg = dict()
    for idbb, start_addr, end_addr in bbs:
        node = dict()
        node['record_id'] = [idbb]
        node['start_addr'] = [start_addr]
        node['end_addr'] = [end_addr]
        node['Edge'] = list(dict_edge.get(idbb, set()))
        cfg[idbb] = node

    # convert instruction information to dictionary {bb: [instructions]}
    ins_dict = dict()
    for idins, opcode, instruction, id_bb in instructions:
        if not id_bb in ins_dict:
            ins_dict[id_bb] = list()
        ins_dict[id_bb].append(instruction)

    depth = 5
    res = dict()
    for target in targets:
        recursive_append_node(res, cfg, target[2], depth)

    # hit count and instructions
    for node in res:
        res[node]['count'] = [bb_cov.get(node, 0)]
        cfg[node]['Instructions'] = ins_dict.get(node, [])

    data = list(res.values())

    return render_template('graph.html', data=data, targets=targets)

@app.route('/fuzzer-stats')
def fuzzer_stats():
    """Render website's home page."""

    names = [
        'ID',
        'START_TIME',
        'LAST_UPDATE',
        'FUZZER_PID',
        'CYCLES_DONE',
        'EXECS_DONE',
        'EXECS_PER_SEC',
        'PATHS_TOTAL',
        'PATHS_FAVORED',
        'PATHS_FOUND',
        'PATHS_IMPORTED',
        'MAX_DEPTH',
        'CUR_PATH',
        'PENDING_FAVS',
        'PENDING_TOTAL',
        'VARIABLE_PATHS',
        'STABILITY',
        'BITMAP_CVG',
        'UNIQUE_CRASHES',
        'UNIQUE_HANGS',
        'LAST_PATH',
        'LAST_CRASH',
        'LAST_HANG',
        'EXECS_SINCE_CRASH',
        'EXEC_TIMEOUT',
        'AFL_BANNER',
        'AFL_VERSION',
        'TARGET_MODE',
        'COMMAND_LINE'
    ]

    db=sqlite3.connect(app.config['DB_FILE'])
    cur = db.cursor()
    cur.execute('select * from fuzzer_stats order by idfuzzer_stats desc')
    res = cur.fetchone()

    stats = zip(names, res)

    return render_template('stats.html', stats=stats)


@app.route('/upload-testcase/')
def upload_testcase():
    """Render the website's about page."""
    return render_template('home.html')


@app.route('/update-targets', methods=['POST', 'GET'])
def update_targets():
    user_form = UserForm()

    # get all targets
    db=sqlite3.connect(app.config['DB_FILE'])
    cur = db.cursor()
    cur.execute('select id_bb, target_address, start_addr, end_addr from targets, bb where targets.id_bb = bb.idbb')
    targets = cur.fetchall()
    target_bb_ids = [target[0] for target in targets]

    if request.method == 'GET':
        return render_template('update_targets.html', targets=targets, form=user_form)

    if user_form.validate_on_submit():
            # Get validated data from form
            try:
                target_addr = int(user_form.target.data, 16)
            except ValueError:
                flash('Invalid Target Address')
                return render_template('update_targets.html', targets=targets, form=user_form)

            # get all basic blocks
            cur.execute('select idbb, start_addr, end_addr from bb order by start_addr')
            bbs = cur.fetchall()
            for bb in bbs:
                if target_addr < bb[1]:
                    continue

                if target_addr < bb[2]:
                    found = True
                    target_bb = bb
                    break

            if not found:
                flash('Target address does not belong to any basic blocks')
                return render_template('update_targets.html', targets=targets, form=user_form)

            # save user to database
            if target_bb[0] in target_bb_ids:
                flash('The Basic Block contains the specified target address is already in list')
            else:
                cur.execute('insert into targets(target_address, id_bb) values (?, ?)', [target_addr, target_bb[0]])
                db.commit()
                targets.append((target_bb[0], target_addr, target_bb[1], target_bb[2]))
                flash('Success')
    else:
        flash_errors(user_form)

    return render_template('update_targets.html', targets=targets, form=user_form)


@app.route('/set-range', methods=['POST', 'GET'])
def set_range():
    user_form = UserForm()
    # if request.method == 'POST':
    #     if user_form.validate_on_submit():
    #         # Get validated data from form
    #         name = user_form.name.data # You could also have used request.form['name']
    #         email = user_form.email.data # You could also have used request.form['email']

    #         # save user to database
    #         user = User(name, email)
    #         db.session.add(user)
    #         db.session.commit()

    #         flash('User successfully added')
    #         return redirect(url_for('home.html'))

    # flash_errors(user_form)
    return render_template('home.html', form=user_form)

@app.route('/add-constraint')
def add_constraint():
    """Render website's home page."""
    return render_template('home.html')


# Flash errors from the form if validation fails
def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (
                getattr(form, field).label.text,
                error
            ))

###
# The functions below should be applicable to all Flask apps.
###

@app.route('/<file_name>.txt')
def send_text_file(file_name):
    """Send your static text file."""
    file_dot_text = file_name + '.txt'
    return app.send_static_file(file_dot_text)


@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=600'
    return response


@app.errorhandler(404)
def page_not_found(error):
    """Custom 404 page."""
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port="8080")
