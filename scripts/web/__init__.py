import os
import sqlite3
from flask import Flask

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['basedir'] = os.path.abspath(os.path.dirname(__file__))
app.config['DB_FILE'] = os.environ['DB_FILE']
app.config['SECRET_KEY'] = 'super secret key'

app.config.from_object(__name__)
from web import views
