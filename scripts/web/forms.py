from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import IntegerField
from wtforms.validators import InputRequired

class UserForm(FlaskForm):
    # target = IntegerField('Target Address(in hex value like 0xDEADBEEF)', validators=[InputRequired()])
    target = StringField('Target Address(in hex value like 0xDEADBEEF)', validators=[InputRequired()])
