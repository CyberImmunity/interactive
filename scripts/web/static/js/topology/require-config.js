require.config({
    paths: {
        colors: '/static/js/topology/colors',
        intermap: '/static/js/topology/intermap',
        locations: '/static/js/topology/locations',
        'polar-layout': '/static/js/topology/polar-layout',
        asgraph: '/static/js/topology/asgraph',
        'triangle-solver': '/static/js/topology/triangle-solver',
        '.cities': '/static/js/topology/.cities',

        babel: '//cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser',
        jsx: '//cdn.rawgit.com/podio/requirejs-react-jsx/v1.0.2/jsx',
        text: '//cdnjs.cloudflare.com/ajax/libs/require-text/2.0.12/text',

        react: '//cdnjs.cloudflare.com/ajax/libs/react/0.14.6/react-with-addons',
        'react-dom': '//cdnjs.cloudflare.com/ajax/libs/react/0.14.6/react-dom',
        'react-bootstrap': '//cdnjs.cloudflare.com/ajax/libs/react-bootstrap/0.28.1/react-bootstrap',

        jquery: '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery',
        bootstrap: '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap',
        underscore: '//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min',
        d3: '//cdnjs.cloudflare.com/ajax/libs/d3/3.5.12/d3',

        cytoscape: '//cdnjs.cloudflare.com/ajax/libs/cytoscape/2.5.5/cytoscape',
        'cytoscape-panzoom': '//cdnjs.cloudflare.com/ajax/libs/cytoscape-panzoom/2.2.0/cytoscape-panzoom'

    },
    shim: {
        bootstrap: ['jquery'],
        'cytoscape-panzoom': ['jquery']
    },
    config: {
        babel: {
            fileExtension: ".jsx"
        }
    }
});
