#!/bin/bash
set -x

function start_container() {
    if [[ "$#" -ne 1 && "$#" -ne 2 ]]; then
        echo "usage:"
        echo "bash ${$0} target_binary [output_file]"
    fi

    if [[ ! -f "${1}" ]]; then
        echo "target binary not found"
        exit 1
    fi

    REALPATH=$(realpath ${1})
    TARGET="$(basename ${REALPATH})"
    VOLUMN="$(dirname ${REALPATH})"
    script_dir="${IFDIR}/scripts/"


    if [[ ! -d "${script_dir}" ]]; then
        echo "interactive fuzzing home not found, check env IFDIR"
        exit 2
    fi

    if [[ ! -f "${script_dir}/generate_cfg_angr.py" ]]; then
        echo "python script not found"
        exit 3
    fi

    if [[ $# -eq 2 ]]; then
        if [[ -d "${2}" ]]; then
            OUT_DIR="$(realpath ${2})"
            OUT_FILE="${TARGET}_cfg_angr.json"
        else
            OUT_DIR="$(realpath $(dirname ${2}))"
            OUT_FILE="$(basename ${2})"
            if [[ ! -d "${OUT_DIR}" ]]; then
                mkdir -p ${OUT_DIR}
                if [[ $? -ne 0 ]]; then
                    echo "output directory cannot be created"
                    exit 6
                fi
            fi
        fi

        docker run -h angr -u ${UID} --name angr -t --rm \
            -v ${VOLUMN}:/tmp/target \
            -v ${OUT_DIR}:/tmp/output \
            -v ${script_dir}:/tmp/scripts \
            angr/angr:latest \
            bash /tmp/scripts/generate_cfg_angr.sh ${TARGET} ${OUT_FILE}
    else
        docker run -h angr -u ${UID} --name angr -t --rm \
            -v ${VOLUMN}:/tmp/target \
            -v ${script_dir}:/tmp/scripts \
            angr/angr:latest \
            bash /tmp/scripts/generate_cfg_angr.sh ${TARGET}
    fi

}

function run_cmd() {
    source /home/angr/.virtualenvs/angr/bin/activate
    # pip install matplotlib
    if [[ $# -eq 1 ]]; then
        python /tmp/scripts/generate_cfg_angr.py -b /tmp/target/${1}
    else
        python /tmp/scripts/generate_cfg_angr.py -b /tmp/target/${1} -o /tmp/output/${2}
    fi
}

#!/bin/bash
if [ -f /.dockerenv ]; then
    run_cmd $@
else
    start_container $@
fi
